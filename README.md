# Página Web Personal
>  Página personal con **HTML5**, **CSS3**, **Bootstrap** y **Javacript**.

[![Curso](https://media-exp1.licdn.com/dms/image/C4E22AQHNjxwij08KeA/feedshare-shrink_800/0?e=1588809600&v=beta&t=EmQEUUVwyf-Rr3ci7pJDeKGIPvRlbsK-xaFWn01xp0g "Curso")](https://media-exp1.licdn.com/dms/image/C4E22AQHNjxwij08KeA/feedshare-shrink_800/0?e=1588809600&v=beta&t=EmQEUUVwyf-Rr3ci7pJDeKGIPvRlbsK-xaFWn01xp0g "Curso")

### Descripción:
Página personal para mostrar mi blog, portafolio y conocimientos. Utilizando mis conocimientos en **Diseño Web**.

## Contacto: 

> Platzi.com/@JuanGalvis

> Twitter.com/@JuaneGalvis

> Instagram.com/Juanesgalvisb

> Linkedin.com/in/Juanegalvis
